/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import { NetworkService } from './services/NetworkService';
export class RtnClient {

    public readonly network: NetworkService;

    constructor() {
        this.network = new NetworkService();
    }
}
