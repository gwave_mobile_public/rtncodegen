/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ExtractZoneBean = {
    key: string;
    name: string;
    picPaths: Array<string>;
    currencies?: Record<string, any> | null;
    order: number;
    status: number;
    leadingCurrency?: string | null;
    fallCount: number;
    leadingPercent: number;
    riseCount: number;
    percent: number;
    bars?: Array<string> | null;
};

