/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ExtractZoneBean } from './ExtractZoneBean';

export type RtnExtractZoneBean = {
    data: Array<ExtractZoneBean>;
    isSuccess: boolean | null;
    eventUpdateEventId?: string;
};

