/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ExtractMarketData } from './ExtractMarketData';

export type RtnMarketSymbols = {
    data: Array<ExtractMarketData>;
    isSuccess: boolean | null;
    eventUpdateEventId?: string;
};

