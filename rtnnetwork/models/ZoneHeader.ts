/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ZoneHeader = {
    allRiseAndFall: number;
    riseCount: number;
    fallCount: number;
    leadingCurrency: string;
    leadingCurrencyRise: number;
};

