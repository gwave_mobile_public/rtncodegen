/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ExtractZoneDetail } from './ExtractZoneDetail';

export type RtnExtractZoneDetail = {
    data: ExtractZoneDetail;
    isSuccess: boolean | null;
    eventUpdateEventId?: string;
};

