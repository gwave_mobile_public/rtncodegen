/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ExtractMarketData } from './ExtractMarketData';
import type { ZoneHeader } from './ZoneHeader';

export type ExtractZoneDetail = {
    header: ZoneHeader;
    symbolList: Array<ExtractMarketData>;
};

