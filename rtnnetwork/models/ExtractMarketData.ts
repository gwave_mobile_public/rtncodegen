/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * ExtractMarketData
 */
export type ExtractMarketData = {
    sort: number;
    coinCode: string;
    priceLast: string;
    volume: string;
    quoteVolume: string;
    riseAndFall: string;
    highPrice: string;
    lowPrice: string;
    precision: number;
    volumePrecision: number;
    marketPricePrecision: number;
    tradePrecision: number;
    tradeInputPrecision: number;
    tradeVolumePrecision: number;
    symbolPrecision: number;
    orderBaseMax: number;
    marketFreezeBuffer: number;
    orderBaseMin: number;
    orderQuoteMax: number;
    orderQuoteMin: number;
    ask: number;
    bid: number;
    orderMax: number;
    orderMin: number;
    symbol: string;
    priceList: Array<number>;
    priceLastRise: number;
    baseCurrencyKey: string;
    allowTrade: boolean;
    state: number;
};

