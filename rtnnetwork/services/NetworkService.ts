/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import { NormalScopeIdManager } from 'platform-kiki-rtn/src/ScopeManager';
import { RtnResponenceInstance } from 'platform-kiki-rtn/src/HttpRequest'
import type { RtnExtractZoneBean } from '../models/RtnExtractZoneBean';
import type { RtnExtractZoneDetail } from '../models/RtnExtractZoneDetail';
import type { RtnMarketSymbols } from '../models/RtnMarketSymbols';
import { PlatformKikiMarketRtn } from 'platform-kiki-rtn/src/NativeBridgeModule'


export class NetworkService {
    constructor() {}

    /**
     * @param routeKey routeKey
     * @param symbolNames symbolNames null 表示获取全部
     * @returns RtnMarketSymbols success response
     */
    public getMarketDataDetail(
        routeKey: string,
        symbolNames: Array<string> | null,
    ) {
        const requestMethod = async () => {
            const realScopeId = await NormalScopeIdManager.getScopeIdByRouteKey(routeKey) as string
            const netResult = await PlatformKikiMarketRtn.getMarketDataDetail(realScopeId, symbolNames)
            let addRemoteListener = undefined
            if (!!netResult?.eventUpdateEventId) {
                addRemoteListener = RtnResponenceInstance.addRemoteListenerHoc<RtnMarketSymbols>(netResult.eventUpdateEventId)
            }
            return RtnResponenceInstance.transformResult<RtnMarketSymbols>(netResult as any, addRemoteListener)
        }
        return requestMethod()
    }

    /**
     * @param routeKey routeKey
     * @returns RtnMarketSymbols success response
     */
    public getOfflineMarketDataDetail(
        routeKey: string,
    ) {
        const requestMethod = async () => {
            const realScopeId = await NormalScopeIdManager.getScopeIdByRouteKey(routeKey) as string
            const netResult = await PlatformKikiMarketRtn.getOfflineMarketDataDetail(realScopeId)
            let addRemoteListener = undefined
            if (!!netResult?.eventUpdateEventId) {
                addRemoteListener = RtnResponenceInstance.addRemoteListenerHoc<RtnMarketSymbols>(netResult.eventUpdateEventId)
            }
            return RtnResponenceInstance.transformResult<RtnMarketSymbols>(netResult as any, addRemoteListener)
        }
        return requestMethod()
    }

    /**
     * @param routeKey routeKey
     * @param maxRefreshTimeout maxRefreshTimeout
     * @returns RtnExtractZoneBean success response
     */
    public getZoneListDetails(
        routeKey: string,
        maxRefreshTimeout: number,
    ) {
        const requestMethod = async () => {
            const realScopeId = await NormalScopeIdManager.getScopeIdByRouteKey(routeKey) as string
            const netResult = await PlatformKikiMarketRtn.getZoneListDetails(realScopeId, maxRefreshTimeout)
            let addRemoteListener = undefined
            if (!!netResult?.eventUpdateEventId) {
                addRemoteListener = RtnResponenceInstance.addRemoteListenerHoc<RtnExtractZoneBean>(netResult.eventUpdateEventId)
            }
            return RtnResponenceInstance.transformResult<RtnExtractZoneBean>(netResult as any, addRemoteListener)
        }
        return requestMethod()
    }

    /**
     * @param routeKey routeKey
     * @param zoneKey zoneKey
     * @param sortName sortName
     * @param sortType sortType
     * @returns RtnExtractZoneDetail success response
     */
    public getZoneDetail(
        routeKey: string,
        zoneKey: string,
        sortName: string,
        sortType: string,
    ) {
        const requestMethod = async () => {
            const realScopeId = await NormalScopeIdManager.getScopeIdByRouteKey(routeKey) as string
            const netResult = await PlatformKikiMarketRtn.getZoneDetail(realScopeId, zoneKey, sortName, sortType)
            let addRemoteListener = undefined
            if (!!netResult?.eventUpdateEventId) {
                addRemoteListener = RtnResponenceInstance.addRemoteListenerHoc<RtnExtractZoneDetail>(netResult.eventUpdateEventId)
            }
            return RtnResponenceInstance.transformResult<RtnExtractZoneDetail>(netResult as any, addRemoteListener)
        }
        return requestMethod()
    }

    /**
     * @param routeKey routeKey
     * @param zoneKey zoneKey
     * @param sortName sortName
     * @param sortType sortType
     * @returns RtnMarketSymbols success response
     */
    public getZoneDetailSymbols(
        routeKey: string,
        zoneKey: string,
        sortName: string,
        sortType: string,
    ) {
        const requestMethod = async () => {
            const realScopeId = await NormalScopeIdManager.getScopeIdByRouteKey(routeKey) as string
            const netResult = await PlatformKikiMarketRtn.getZoneDetailSymbols(realScopeId, zoneKey, sortName, sortType)
            let addRemoteListener = undefined
            if (!!netResult?.eventUpdateEventId) {
                addRemoteListener = RtnResponenceInstance.addRemoteListenerHoc<RtnMarketSymbols>(netResult.eventUpdateEventId)
            }
            return RtnResponenceInstance.transformResult<RtnMarketSymbols>(netResult as any, addRemoteListener)
        }
        return requestMethod()
    }

    /**
     * @param routeKey routeKey
     * @param num num
     * @returns RtnExtractZoneBean success response
     */
    public getMarketDetailTabZoneDataList(
        routeKey: string,
        num: number,
    ) {
        const requestMethod = async () => {
            const realScopeId = await NormalScopeIdManager.getScopeIdByRouteKey(routeKey) as string
            const netResult = await PlatformKikiMarketRtn.getMarketDetailTabZoneDataList(realScopeId, num)
            let addRemoteListener = undefined
            if (!!netResult?.eventUpdateEventId) {
                addRemoteListener = RtnResponenceInstance.addRemoteListenerHoc<RtnExtractZoneBean>(netResult.eventUpdateEventId)
            }
            return RtnResponenceInstance.transformResult<RtnExtractZoneBean>(netResult as any, addRemoteListener)
        }
        return requestMethod()
    }

    /**
     * @param routeKey routeKey
     * @param baseCurrency baseCurrency
     * @param zoneKey zoneKey
     * @param sortName sortName
     * @param sortType sortType
     * @returns RtnMarketSymbols success response
     */
    public getMarketDetailByCondition(
        routeKey: string,
        baseCurrency: string,
        zoneKey: string,
        sortName: string,
        sortType: string,
    ) {
        const requestMethod = async () => {
            const realScopeId = await NormalScopeIdManager.getScopeIdByRouteKey(routeKey) as string
            const netResult = await PlatformKikiMarketRtn.getMarketDetailByCondition(realScopeId, baseCurrency, zoneKey, sortName, sortType)
            let addRemoteListener = undefined
            if (!!netResult?.eventUpdateEventId) {
                addRemoteListener = RtnResponenceInstance.addRemoteListenerHoc<RtnMarketSymbols>(netResult.eventUpdateEventId)
            }
            return RtnResponenceInstance.transformResult<RtnMarketSymbols>(netResult as any, addRemoteListener)
        }
        return requestMethod()
    }

}
