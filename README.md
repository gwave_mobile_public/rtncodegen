# rtncodegen

``` bash
npm install -g git@gitlab.com:Keccak256-evg/fe-platform/templatecli/kikicli.git
npm install -g git@gitlab.com:Keccak256-evg/fe-platform/lowcode/openapi-typescript-codegen.git
cd ./dev-modules/platformkikirtn
kkrn apijson -o ../rtncodegen/testjson.json

cd ..
openapi --input ./rtncodegen/testjson.json --output ./rtncodegen/rtnnetwork --name RtnClient --type rtn

```
